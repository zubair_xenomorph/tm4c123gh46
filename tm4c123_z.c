#include "stdint.h"
#include "lm4f120h5qr.h"
#include "tm4c123_z.h"

#define GPIOA_APB GPIO_PORTA_DATA_BITS_R
#define GPIOA_AHB GPIO_PORTA_AHB_DATA_BITS_R
#define GPIOB_APB GPIO_PORTB_DATA_BITS_R
#define GPIOB_AHB GPIO_PORTB_AHB_DATA_BITS_R
#define GPIOC_APB GPIO_PORTC_DATA_BITS_R
#define GPIOC_AHB GPIO_PORTC_AHB_DATA_BITS_R
#define GPIOD_APB GPIO_PORTD_DATA_BITS_R
#define GPIOD_AHB GPIO_PORTD_AHB_DATA_BITS_R
#define GPIOE_APB GPIO_PORTE_DATA_BITS_R
#define GPIOE_AHB GPIO_PORTE_AHB_DATA_BITS_R
#define GPIOF_APB GPIO_PORTF_DATA_BITS_R
#define GPIOF_AHB GPIO_PORTF_AHB_DATA_BITS_R

#define GPIODATA 0x3FC
#define GPIODIR 0x400
#define GPIOIS 0x404
#define GPIOIBE 0x408
#define GPIOIEV 0x40C
#define GPIOIM 0x410
#define GPIORIS 0x414
#define GPIOMIS 0x418
#define GPIOICR 0x41C
#define GPIOAFSEL 0x420
#define GPIODR2R 0x500
#define GPIODR4R 0x504
#define GPIODR8R 0x508
#define GPIOODR 0x50C
#define GPIOPUR 0x510
#define GPIOPDR 0x514
#define GPIOSLR 0x518
#define GPIODEN 0x51C
#define GPIOLOCK 0x520
#define GPIOCR 0x524
#define GPIOAMSEL 0x528
#define GPIOPCTL 0x52C
#define GPIOADCCTL 0x530
#define GPIODMACTL 0x534
#define GPIOPeriphID4 0xFD0
#define GPIOPeriphID5 0xFD4
#define GPIOPeriphID6 0xFD8
#define GPIOPeriphID7 0xFDC
#define GPIOPeriphID0 0xFE0
#define GPIOPeriphID1 0xFE4
#define GPIOPeriphID2 0xFE8
#define GPIOPeriphID3 0xFEC
#define GPIOPCellID0 0xFF0
#define GPIOPCellID1 0xFF4
#define GPIOPCellID2 0xFF8
#define GPIOPCellID3 0xFFC


/*


typedef struct
{
  volatile const unsigned long* GPIOA;
  const unsigned long* GPIOB;
  const unsigned long* GPIOC = GPIOC_APB;
  const unsigned long* GPIOD = GPIOD_APB;
  const unsigned long* GPIOE = GPIOE_APB;
  const unsigned long* GPIOF = GPIOF_APB;
  
} GPIO_APB;

typedef struct
{
  const unsigned long* GPIOA = (unsigned long)GPIOA_AHB;
  const unsigned long* GPIOB = GPIOB_AHB;
  const unsigned long* GPIOC = GPIOC_AHB;
  const unsigned long* GPIOD = GPIOD_AHB;
  const unsigned long* GPIOE = GPIOE_AHB;
  const unsigned long* GPIOF = GPIOF_AHB;
  
} GPIO_AHB;

*/

void GPIO_write(volatile unsigned long* GPIOx_AxB, uint8_t data)
{
  if(GPIOx_AxB==GPIOA_APB||GPIOx_AxB==GPIOA_AHB) SYSCTL_RCGCGPIO_R = 0x01;
  else if(GPIOx_AxB==GPIOB_APB||GPIOx_AxB==GPIOB_AHB) SYSCTL_RCGCGPIO_R = 0x02;
  else if(GPIOx_AxB==GPIOC_APB||GPIOx_AxB==GPIOC_AHB) SYSCTL_RCGCGPIO_R = 0x04;
  else if(GPIOx_AxB==GPIOD_APB||GPIOx_AxB==GPIOD_AHB) SYSCTL_RCGCGPIO_R = 0x08;
  else if(GPIOx_AxB==GPIOE_APB||GPIOx_AxB==GPIOE_AHB) SYSCTL_RCGCGPIO_R = 0x10;
  else if(GPIOx_AxB==GPIOF_APB||GPIOx_AxB==GPIOF_AHB) SYSCTL_RCGCGPIO_R = 0x20;
  *(GPIOx_AxB+GPIODEN/4)=data;
  *(GPIOx_AxB+GPIODIR/4)=data;
  *(GPIOx_AxB+GPIODATA/4)=data;
}

uint8_t GPIO_read(volatile unsigned long* GPIOx_AxB, uint8_t msk)
{
  if(GPIOx_AxB==GPIOA_APB||GPIOx_AxB==GPIOA_AHB) SYSCTL_RCGCGPIO_R = 0x01;
  else if(GPIOx_AxB==GPIOB_APB||GPIOx_AxB==GPIOB_AHB) SYSCTL_RCGCGPIO_R = 0x02;
  else if(GPIOx_AxB==GPIOC_APB||GPIOx_AxB==GPIOC_AHB) SYSCTL_RCGCGPIO_R = 0x04;
  else if(GPIOx_AxB==GPIOD_APB||GPIOx_AxB==GPIOD_AHB) SYSCTL_RCGCGPIO_R = 0x08;
  else if(GPIOx_AxB==GPIOE_APB||GPIOx_AxB==GPIOE_AHB) SYSCTL_RCGCGPIO_R = 0x10;
  else if(GPIOx_AxB==GPIOF_APB||GPIOx_AxB==GPIOF_AHB) SYSCTL_RCGCGPIO_R = 0x20;
  *(GPIOx_AxB+GPIODEN/4)=msk;
  *(GPIOx_AxB+GPIODIR/4)=!msk;
  return *(GPIOx_AxB+GPIODATA/4);
}
