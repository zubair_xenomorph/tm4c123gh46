#ifndef __TM4C123_Z_H__
#define __TM4C123_Z_H__

void GPIO_write(volatile unsigned long* GPIOx_AxB, uint8_t data);
int8_t GPIO_read(volatile unsigned long* GPIOx_AxB, uint8_t msk);

#endif // __TM4C123_Z_H__